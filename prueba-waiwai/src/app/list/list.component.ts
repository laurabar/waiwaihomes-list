import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {

  data1 = data1;
  data2 = data2;
  data3 = data3;
  data4 = data4;
  data5 = data5;

  constructor() { }

  ngOnInit(): void {}
}

const data1 = [
  {srcUrl: "../assets/test01/img1.jpg", previewUrl: "../assets/test01/img1.jpg"},
  {srcUrl: "../assets/test01/img2.jpg", previewUrl: "../assets/test01/img2.jpg"},
  {srcUrl: "../assets/test01/img3.jpg", previewUrl: "../assets/test01/img3.jpg"},
  {srcUrl: "../assets/test01/img4.jpg", previewUrl: "../assets/test01/img4.jpg"},
  {srcUrl: "../assets/test01/img5.jpg", previewUrl: "../assets/test01/img5.jpg"}
]
const data2 = [
  {srcUrl: "../assets/test02/img1.jpg", previewUrl: "../assets/test02/img1.jpg"},
  {srcUrl: "../assets/test02/img2.jpg", previewUrl: "../assets/test02/img2.jpg"},
  {srcUrl: "../assets/test02/img3.jpg", previewUrl: "../assets/test02/img3.jpg"},
  {srcUrl: "../assets/test02/img4.jpg", previewUrl: "../assets/test02/img4.jpg"},
  {srcUrl: "../assets/test02/img5.jpg", previewUrl: "../assets/test02/img5.jpg"}
]
const data3 = [
  {srcUrl: "../assets/test03/img1.jpg", previewUrl: "../assets/test03/img1.jpg"},
  {srcUrl: "../assets/test03/img2.jpg", previewUrl: "../assets/test03/img2.jpg"},
  {srcUrl: "../assets/test03/img3.jpg", previewUrl: "../assets/test03/img3.jpg"},
  {srcUrl: "../assets/test03/img4.jpg", previewUrl: "../assets/test03/img4.jpg"},
  {srcUrl: "../assets/test03/img5.jpg", previewUrl: "../assets/test03/img5.jpg"}
]
const data4 = [
  {srcUrl: "../assets/test04/img1.jpg", previewUrl: "../assets/test04/img1.jpg"},
  {srcUrl: "../assets/test04/img2.jpg", previewUrl: "../assets/test04/img2.jpg"},
  {srcUrl: "../assets/test04/img3.jpg", previewUrl: "../assets/test04/img3.jpg"},
  {srcUrl: "../assets/test04/img4.jpg", previewUrl: "../assets/test04/img4.jpg"},
  {srcUrl: "../assets/test04/img5.jpg", previewUrl: "../assets/test04/img5.jpg"}
]
const data5 = [
  {srcUrl: "../assets/test05/img1.jpg", previewUrl: "../assets/test05/img1.jpg"},
  {srcUrl: "../assets/test05/img2.jpg", previewUrl: "../assets/test05/img2.jpg"},
  {srcUrl: "../assets/test05/img3.jpg", previewUrl: "../assets/test05/img3.jpg"},
  {srcUrl: "../assets/test05/img4.jpg", previewUrl: "../assets/test05/img4.jpg"},
  {srcUrl: "../assets/test05/img5.jpg", previewUrl: "../assets/test05/img5.jpg"}
]
