# PruebaWaiwai

alias ng="/Users/laurabarber/.npm-global/bin/ng"

npm install
npm install --save @woocommerce/woocommerce-rest-api 
npm install --save bootstrap
npm install --save jquery
npm install --save @fortawesome/fontawesome-free
npm install --save @angular/material
npm install --save angular-bootstrap-md
npm install --save popper
npm install --save hammerjs
npm install --save http-client
npm install --save mdbootstrap
npm install -–save chart.js@2.5.0 @types/chart.js animate.css



This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
